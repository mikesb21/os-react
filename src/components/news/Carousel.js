import React from 'react';
import BackgroundImage from '../../images/bg.png';

const Carousel = ({ carouselArticles }) => {
  return (
    <div id="myCarousel" className="carousel slide" data-ride="carousel">
      <ol className="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" className=""></li>
        <li data-target="#myCarousel" data-slide-to="1" className="active"></li>
        <li data-target="#myCarousel" data-slide-to="2" className=""></li>
      </ol>
      <div className="carousel-inner">
        <div className="carousel-item">
          <img src={BackgroundImage} alt="..." />
          <div className="container">
            <div className="carousel-caption">
              <h1>{carouselArticles[0].title}</h1>
              <p>
                <a
                  className="btn btn-lg btn-primary"
                  target="_blank"
                  rel="noopener noreferrer"
                  href={carouselArticles[0].url}
                  role="button"
                >
                  Read More
                </a>
              </p>
            </div>
          </div>
        </div>
        <div className="carousel-item active">
          <img src={BackgroundImage} alt="..." />
          <div className="container">
            <div className="carousel-caption">
              <h1>{carouselArticles[1].title}</h1>
              <p>
                <a
                  className="btn btn-lg btn-primary"
                  target="_blank"
                  rel="noopener noreferrer"
                  href={carouselArticles[1].url}
                  role="button"
                >
                  Read more
                </a>
              </p>
            </div>
          </div>
        </div>
        <div className="carousel-item">
          <img src={BackgroundImage} alt="..." />
          <div className="container">
            <div className="carousel-caption">
              <h1>{carouselArticles[2].title}</h1>
              <p>
                <a
                  className="btn btn-lg btn-primary"
                  target="_blank"
                  rel="noopener noreferrer"
                  href={carouselArticles[2].url}
                  role="button"
                >
                  Read more
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
      <a
        className="carousel-control-prev"
        href="#myCarousel"
        role="button"
        data-slide="prev"
      >
        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
        <span className="sr-only">Previous</span>
      </a>
      <a
        className="carousel-control-next"
        href="#myCarousel"
        role="button"
        data-slide="next"
      >
        <span className="carousel-control-next-icon" aria-hidden="true"></span>
        <span className="sr-only">Next</span>
      </a>
    </div>
  );
};

export default Carousel;
