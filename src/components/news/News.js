import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Spinner from '../common/Spinner';
import Article from './Article';
import Carousel from './Carousel';

const News = () => {
  const [articles, setArticles] = useState([]);
  const [load, setLoad] = useState(false);
  const [error, setError] = useState('');

  useEffect(() => {
    axios
      .get(
        `http://newsapi.org/v2/top-headlines?country=us&category=technology&sortBy=publishedAt&apiKey=${process.env.REACT_APP_API_KEY}`
      )
      .then((res) => {
        const data = res.data.articles;
        setArticles(data);
        setLoad(true);
      })
      .catch((err) => {
        setError(err.message);
        setLoad(true);
      });
  }, []);

  return (
    <div>
      {console.log()}
      {load ? (
        error ? (
          <h6>{error.message}</h6>
        ) : (
          <div>
            <Carousel carouselArticles={articles.slice(1, 4)} />
            <div className="container d-flex justify-content-center">
              <Article articles={articles} />
            </div>
          </div>
        )
      ) : (
        <Spinner />
      )}
    </div>
  );
};

export default News;
