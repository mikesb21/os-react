import React from 'react';

const Article = ({ articles }) => {
  return (
    <div className="container-fluid content-row">
      <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-5 align-items-stretch">
        {articles.map((article, index) => (
          <div key={index} className="card mb-4" style={{ margin: '1rem 0' }}>
            <img
              src={article.urlToImage}
              className="card-img-top img-fluid"
              alt="..."
            ></img>
            <div className="card-body d-flex flex-column">
              <h5 className="card-title">
                <a href={article.url} target="_blank" rel="noopener noreferrer">
                  {article.title}
                </a>
              </h5>
              <p id="cardText" className="card-text">
                {article.description}
              </p>
              <a
                href={article.url}
                target="_blank"
                rel="noopener noreferrer"
                className="btn btn-primary"
              >
                Read more
              </a>
              <footer
                className="blockquote-footer"
                style={{ marginTop: '10px' }}
              >
                <cite title="published">
                  Published: {article.publishedAt}
                  <p style={{ float: 'right' }}>
                    Source: {article.source.name}
                  </p>
                </cite>
              </footer>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Article;
