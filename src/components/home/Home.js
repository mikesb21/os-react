import React from 'react';
import { Link } from 'react-router-dom';

const Home = () => {
  return (
    <div className="jumbotron">
      <h1>Tech News</h1>
      <p>React and React Router for ultra-responsive web apps</p>
      <Link to="/about" className="btn btn-primary btn-lg">
        Learn More
      </Link>
    </div>
  );
};

export default Home;
