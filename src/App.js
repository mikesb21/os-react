import React from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import Header from './components/common/Header';
import Home from './components/home/Home';
import About from './components/about/About';
import News from './components/news/News';
import PageNotFound from './components/pageNotFound/PageNotFound';

const App = () => (
  <div>
    <Header />
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/about" component={About} />
      <Route path="/news" component={News} />
      <Route component={PageNotFound} />
    </Switch>
  </div>
);

export default App;
